Contributing Guidelines
=======================

All contributions should be submitted in form of merge requests targeting
the branch *master*. They will be subject to automatic checks run by
Gitlab-CI jobs, and they will be reviewed by project maintainers.

We do not use tags, because the LHCb Nightly Builds System will automatically
pick up the *HEAD* commit of the *master* branch every night around midnight,
or whenever a new build of a slot is requested.


Adding a new slot
-----------------

New slots have to be created in one of the existing Python modules in the
package ``lhcbnightlyconf``, or in a new module if none of the existing
ones are appropriate.  In all cases the new slot must be added to the
conventional list called ``slots``.

When a new module is created, make sure the module is imported in the top
level ``__init__.py`` and its ``slots`` list is added to the global
``slots`` list.

If a new slot is almost identical to an existing one, consider, if not yet
done, factoring out the common code in a *slot factory function*.


Slot factory functions
----------------------

When multiple slots share a good fraction of the construction code, it is wise
to use a *slot factory function*, i.e. a function that based on some parameters
can generate the required specific ``Slot`` instance.

Slot factory functions must be labeled with the ``@slot_factory`` decorator,
for example with something like::

    from LbNightlyTools.Configuration import slot_factory, Slot

    @slot_factory
    def make_my_slot(name):
        return Slot(name)


Testing
-------

Changes to the configuration are automatically validated in a Gitlab-CI job
after every push to the repository, but they can also be tested locally.

From the standard `LHCb User Environment <https://twiki.cern.ch/twiki/bin/view/LHCb/LbEnv>`_
one can just call ``lbn-check-config`` from the top directory of the cloned
git repository.

When the LHCb User Environment is not available, one can prepare a sandboxed
environment using `Poetry <https://poetry.eustace.io/>`_::

    poetry install
    poetry run lbn-check-config


Examples
--------

This show a minimal example of a module defining a couple of slots::

    from LbNightlyTools.Configuration import slot_factory, Slot, Project

    @slot_factory
    def make_my_slot(name, version='master'):
        return Slot(name,
                    projects=[Project('MyProject', version])

    slots = [
        make_my_slot('slot1'),
        make_my_slot('slot2', 'HEAD'),
    ]

An alternative way of adding slots to the ``slots`` list can be::

    from LbNightlyTools.Configuration import slot_factory, Slot, Project

    slots = []

    @slot_factory
    def make_my_slot(name, version='master'):
        return Slot(name,
                    projects=[Project('MyProject', version)])

    s1 = make_my_slot('slot1')
    slots.append(s1)

    s2 = make_my_slot('slot2', 'HEAD')
    slots.append(s2)

The top level ``__init__.py`` may look like this::

    from . import MyModule1
    from . import MyModule2

    slots = MyModule1.slots + MyModule2.slots
