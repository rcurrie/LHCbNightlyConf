###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LbNightlyTools.Configuration import Slot, Project, DBASE, Package
from .Common import WARNINGS_EXCEPTIONS, ERROR_EXCEPTIONS

NAMES = ['LHCb', 'Lbcom', 'Rec', 'Phys', 'Hlt', 'Moore']

moore_2011 = Slot(
    'lhcb-hlt2011-patches',
    desc='Test slot with patches for the 2011 Moore stack',
    projects=[Project(name, 'hlt2011-patches') for name in NAMES],
    platforms=['x86_64-slc5-gcc43-opt', 'x86_64-slc5-gcc43-dbg'],
    warning_exceptions=[r'/Boost/', r'/genreflex/'],
    build_tool='cmt')

moore_2012 = Slot(
    'lhcb-hlt2012-patches',
    desc='Test slot with patches for the 2012 Moore stack',
    projects=[Project(name, 'hlt2012-patches') for name in NAMES],
    platforms=['x86_64-slc5-gcc46-opt', 'x86_64-slc5-gcc46-dbg'],
    warning_exceptions=[r'/Boost/', r'/genreflex/'],
    build_tool='cmt')

for s in [moore_2011, moore_2012]:
    s.env.append('CMTEXTRATAGS=host-slc5')

moore_2016 = Slot(
    'lhcb-hlt2016-patches',
    desc='Test slot with patches for the 2016 Moore stack',
    projects=[
        Project(name, 'hlt2016-patches', checkout='git') for name in NAMES
    ],
    warning_exceptions=list(WARNINGS_EXCEPTIONS),
    error_exceptions=list(ERROR_EXCEPTIONS),
    platforms=['x86_64-slc6-gcc49-opt', 'x86_64-slc6-gcc49-dbg'])

# list of defined slots
slots = [moore_2011, moore_2012, moore_2016]

# Common slot settings
for slot in slots:
    slot.projects.append(DBASE(packages=[Package('PRConfig', 'HEAD')]))
    for p in slot.projects:
        p.checkout_opts['merges'] = 'all'
