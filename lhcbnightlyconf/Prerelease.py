# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Configuration for pre-release full-stack slot.
'''

from LbNightlyTools.Configuration import Slot, Project
from .Common import (ALL_PROJECTS, ALL_PLATFORMS, DEFAULT_ENV,
                     WARNINGS_EXCEPTIONS, ERROR_EXCEPTIONS)

VERSIONS = {
    'LCG': '96b',
    'Gaudi': 'v33r0',
}

RELEASED_PROJECTS = ['LCG']

PLATFORMS = ALL_PLATFORMS

prerelease = Slot(
    'lhcb-prerelease',
    desc='Currently building master stack on top of Gaudi v32r2 and LCG_96b',
    projects=[
        Project(
            name,
            VERSIONS.get(name, 'master'),
            disabled=name in RELEASED_PROJECTS) for name in ALL_PROJECTS
    ],
    platforms=PLATFORMS,
    env=DEFAULT_ENV,
    warning_exceptions=list(WARNINGS_EXCEPTIONS),
    error_exceptions=list(ERROR_EXCEPTIONS))

prerelease.Geant4.with_shared = True
prerelease.cache_entries['GEANT4_TAG'] = 'lhcb/v10.4.2-branch'
prerelease.deployment = []

slots = [prerelease]
