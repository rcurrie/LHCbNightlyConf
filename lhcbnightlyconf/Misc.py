# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LbNightlyTools.Configuration import Slot, Project

import os

from .Main import create_head_slot

slots = []

if 'LBN_GAUDI_MR' in os.environ:
    mreq_iid = int(os.environ['LBN_GAUDI_MR'])
    merge = create_head_slot('lhcb-gaudi-merge')
    merge.disabled = True  # must be started explicitly
    mreq_base_url = 'https://gitlab.cern.ch/gaudi/Gaudi/merge_requests'
    merge.desc = (
        'Build on Gaudi/master plus '
        '<a target="_blank" href="{0}/{1}">gaudi/Gaudi!{1}</a>').format(
            mreq_base_url, mreq_iid)
    merge.Gaudi.version = 'mr{0}'.format(mreq_iid)
    merge.platforms = ['x86_64-centos7-gcc9-opt']
    slots.append(merge)

#-- lhcb-coverity
coverity = create_head_slot(
    'lhcb-coverity',
    projects=[
        'LCG', 'Gaudi', 'LHCb', 'Lbcom', 'Rec', 'Boole', 'Phys', 'Analysis',
        'Stripping', 'DaVinci', 'Geant4', 'Gauss'
    ],
    platforms=['x86_64-centos7-gcc9-opt'])
coverity.desc = (
    'Basic build for Coverity analysis '
    '(go to <a href="https://coverity.cern.ch/" target="_blank">reports</a>)')
coverity.disabled = True
coverity.no_test = True
coverity.warning_exceptions.append(
    r'^\[WARNING\] (No files were emitted|.*compilation units)')
slots.append(coverity)
