# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2019-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LbNightlyTools.Configuration import Slot, Project
from .Common import WARNINGS_EXCEPTIONS, ERROR_EXCEPTIONS

from itertools import starmap

s21_first = Slot(
    'lhcb-stripping21-firstpass-patches',
    desc=
    'Maintenance of a patched version of DaVinci for original Stripping 21 full stripping',
    projects=starmap(Project, [('LHCb', 'stripping21-firstpass-patches'),
                               ('Lbcom', 'stripping21-firstpass-patches'),
                               ('Rec', 'stripping21-firstpass-patches'),
                               ('Phys', 'stripping21-firstpass-patches'),
                               ('Analysis', 'stripping21-firstpass-patches'),
                               ('Stripping', 'stripping21-firstpass-patches'),
                               ('DaVinci', 'stripping21-firstpass-patches')]),
    platforms=['x86_64-slc6-gcc48-opt', 'x86_64-slc6-gcc48-dbg'],
    build_tool='cmt',
    warning_exceptions=list(WARNINGS_EXCEPTIONS),
    error_exceptions=list(ERROR_EXCEPTIONS),
    deployment=[])

for p in s21_first.projects:
    p.checkout_opts['merges'] = 'all'

s21 = Slot(
    'lhcb-stripping21-patches',
    desc=
    'Maintenance of a patched version of DaVinci for Stripping 21 incremental restrippings',
    projects=starmap(Project, [('LHCb', 'stripping21-patches'),
                               ('Lbcom', 'stripping21-patches'),
                               ('Rec', 'stripping21-patches'),
                               ('Phys', 'stripping21-patches'),
                               ('Analysis', 'stripping21-patches'),
                               ('Stripping', 'stripping21-patches'),
                               ('DaVinci', 'stripping21-patches')]),
    platforms=['x86_64-slc6-gcc49-opt', 'x86_64-slc6-gcc49-dbg'],
    warning_exceptions=list(WARNINGS_EXCEPTIONS),
    error_exceptions=list(ERROR_EXCEPTIONS),
    deployment=['cvmfs'])

for p in s21.projects:
    p.checkout_opts['merges'] = 'all'

s24 = Slot(
    'lhcb-stripping24-patches',
    desc='Maintenance of a patched version of DaVinci for Stripping 24',
    projects=starmap(Project, [('LHCb', 'stripping24-patches'),
                               ('Lbcom', 'stripping24-patches'),
                               ('Rec', 'stripping24-patches'),
                               ('Phys', 'stripping24-patches'),
                               ('Analysis', 'stripping24-patches'),
                               ('Stripping', 'stripping24-patches'),
                               ('DaVinci', 'stripping24-patches')]),
    platforms=['x86_64-slc6-gcc49-opt', 'x86_64-slc6-gcc49-dbg'],
    warning_exceptions=list(WARNINGS_EXCEPTIONS),
    error_exceptions=list(ERROR_EXCEPTIONS),
    deployment=['cvmfs'])

for p in s24.projects:
    p.checkout_opts['merges'] = 'all'

s24s28 = Slot(
    'lhcb-stripping24r2-28r2-patches',
    desc=
    'Maintenance of a patched version of DaVinci for Stripping 24r2 and 28r2',
    projects=starmap(Project, [('LHCb', 'stripping24r2-28r2-patches'),
                               ('Lbcom', 'stripping24r2-28r2-patches'),
                               ('Rec', 'stripping24r2-28r2-patches'),
                               ('Phys', 'stripping24r2-28r2-patches'),
                               ('Analysis', 'stripping24r2-28r2-patches'),
                               ('Stripping', 'stripping24r2-28r2-patches'),
                               ('DaVinci', 'stripping24r2-28r2-patches')]),
    platforms=['x86_64-centos7-gcc7-opt', 'x86_64-centos7-gcc7-dbg'],
    warning_exceptions=list(WARNINGS_EXCEPTIONS),
    error_exceptions=list(ERROR_EXCEPTIONS),
    deployment=['cvmfs'])

for p in s24s28.projects:
    p.checkout_opts['merges'] = 'all'

slots = [s21_first, s21, s24, s24s28]
