# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Configuration for main full-stack slots.
'''
from LbNightlyTools.Configuration import (Slot, Project, DBASE, PARAM, Package,
                                          slot_factory)
from .Common import (ALL_PROJECTS, ALL_PLATFORMS, ALL_PLATFORMS_NO_CLANG,
                     DEFAULT_ENV, WARNINGS_EXCEPTIONS, ERROR_EXCEPTIONS,
                     DEFAULT_DBASE_PACKAGES, DEFAULT_CACHE_ENTRIES)
from itertools import starmap

LCG_DEFAULT_VERSION = '96b'


# Slots configuration
@slot_factory
def create_head_slot(name,
                     deploy=False,
                     LCG=LCG_DEFAULT_VERSION,
                     projects=ALL_PROJECTS,
                     platforms=ALL_PLATFORMS):
    slot = Slot(
        name,
        desc='head of everything against Gaudi/HEAD',
        projects=[Project(name, 'HEAD') for name in projects],
        platforms=platforms,
        env=list(DEFAULT_ENV),
        deployment=['cvmfs'] if deploy else [],
        warning_exceptions=list(WARNINGS_EXCEPTIONS),
        error_exceptions=list(ERROR_EXCEPTIONS),
        cache_entries=DEFAULT_CACHE_ENTRIES)
    if hasattr(slot, 'LCG'):
        slot.desc += (
            ' and <a href="http://lcginfo.cern.ch/release/{LCG_version}/" '
            'target="_blank">LCG_{LCG_version}</a>').format(LCG_version=LCG)
        slot.LCG.version = LCG
        slot.LCG.disabled = True
    if hasattr(slot, 'Geant4'):
        slot.Geant4.with_shared = True
    slot.cache_entries['GEANT4_TAG'] = 'lhcb/v10.4.3-branch'
    slot.cache_entries['GEANT4FILES_VERSION'] = 'v104r1'
    slot.env.append(
        'CMAKE_PREFIX_PATH=${PWD}/build/Gaudi/cmake:${CMAKE_PREFIX_PATH}')

    slot.projects.extend([
        DBASE(packages=[
            Package(name, version) for name, version in DEFAULT_DBASE_PACKAGES
        ]),
        PARAM(packages=[
            Package('TMVAWeights', 'HEAD'),
            Package('ParamFiles', 'HEAD'),
        ])
    ])
    return slot


slots = []

gaudi_head = create_head_slot('lhcb-gaudi-head', deploy=True)
gaudi_head.env.append(
    'CMAKE_PREFIX_PATH=/cvmfs/sft.cern.ch/lcg/releases:${CMAKE_PREFIX_PATH}')
slots.append(gaudi_head)

head = create_head_slot('lhcb-head', platforms=ALL_PLATFORMS, deploy=True)
head.Gaudi.version = 'master'
head.desc = head.desc.replace(
    'Gaudi/HEAD',
    '<a href="https://gitlab.cern.ch/gaudi/Gaudi/tree/master" target="_blank">Gaudi/master</a>'
)
slots.append(head)

py3 = create_head_slot(
    'lhcb-gaudi-head-py3',
    projects=[
        'LCG', 'Gaudi', 'LHCb', 'Lbcom', 'Rec', 'Brunel', 'Phys', 'Moore'
    ],
    LCG=LCG_DEFAULT_VERSION + 'python3',
    platforms=['x86_64-centos7-gcc9-opt', 'x86_64-centos7-gcc9-dbg'])
py3.env.append(
    'CMAKE_PREFIX_PATH=/cvmfs/sft.cern.ch/lcg/releases:${CMAKE_PREFIX_PATH}')
slots.append(py3)

# Slot for sanitizer tests
SAN_PROJECTS = [
    'LCG', 'Gaudi', 'LHCb', 'Lbcom', 'Rec', 'Brunel', 'Boole', 'Phys', 'Moore',
    'Analysis', 'Stripping', 'DaVinci', 'Bender', 'Online', 'Panoptes'
]
san = create_head_slot(
    'lhcb-sanitizers',
    projects=SAN_PROJECTS,
    platforms=[
        'x86_64-centos7-gcc9-dbg+asan', 'x86_64-centos7-gcc9-dbg+lsan',
        'x86_64-centos7-gcc9-dbg+ubsan'
    ])
san.desc = san.desc + (
    ' built with '
    '<a href="http://clang.llvm.org/docs/AddressSanitizer.html" target="_blank">address</a>, '
    '<a href="http://clang.llvm.org/docs/LeakSanitizer.html" target="_blank">leak</a> and '
    '<a href="http://clang.llvm.org/docs/UndefinedBehaviorSanitizer.html" target="_blank">undefined behaviour</a> '
    'sanitizers enabled')
san.cache_entries['CMAKE_USE_CCACHE'] = 'NO'
slots.append(san)


# Slot for DD4hep tests
@slot_factory
def create_dd4hep_slot(name, LCG=LCG_DEFAULT_VERSION, deploy=True):
    slot = create_head_slot(
        name,
        projects=[
            'LCG', 'Gaudi', 'Detector', 'LHCb', 'Lbcom', 'Rec', 'Gaussino',
            'Brunel', 'Phys', 'Moore', 'Alignment'
        ],
        platforms=['x86_64-centos7-gcc9-opt', 'x86_64-centos7-gcc9-dbg'],
        LCG=LCG,
        deploy=deploy)
    slot.desc = 'Gaussino and DD4hep prototype slot'
    slot.env.append(
        'CMAKE_PREFIX_PATH=/cvmfs/lhcbdev.cern.ch/lib/lcg/releases:${CMAKE_PREFIX_PATH}'
    )
    slot.cache_entries['DETECTOR_VERSION'] = 'HEAD'
    slot.cache_entries['USE_DD4HEP'] = 'ON'

    slot.Gaudi.version = 'dd4hep_vp'
    slot.LHCb.version = 'dd4hep_vp'
    slot.LHCb._deps = ['Detector']
    slot.LHCb.checkout_opts['merges'] = 'all'
    slot.Lbcom.version = 'dd4hep_vp'
    slot.Rec.version = 'dd4hep_vp'
    slot.Brunel.version = 'dd4hep_vp'
    slot.Moore.version = 'dd4hep_vp'
    slot.Gaussino.version = 'LCG95_dev'
    slot.Gaussino.checkout = 'git'
    slot.Gaussino.checkout_opts[
        'url'] = 'https://gitlab.cern.ch/Gaussino/Gaussino.git'
    slot.cache_entries['USE_DD4HEP'] = 'ON'
    return slot


dd4hep = create_dd4hep_slot('lhcb-dd4hep')
slots.append(dd4hep)
