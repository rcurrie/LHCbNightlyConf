###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function, absolute_import

from itertools import chain

from . import Main
from . import Master
from . import Misc
from . import Boole
from . import G4R10
from . import Gauss
from . import DaVinciTests
from . import lcg_nightlies
from . import MooreLegacy
from . import Patches
from . import Prerelease
from . import Reco14
from . import Reco15
from . import StrippingTests
from . import Temporary
from . import UpgradeTests
from . import Release

slots = []
slots.extend(
    chain(
        Main.slots,
        Master.slots,
        Misc.slots,
        Boole.slots,
        DaVinciTests.slots,
        G4R10.slots,
        Gauss.slots,
        lcg_nightlies.slots,
        MooreLegacy.slots,
        Patches.slots,
        Prerelease.slots,
        Reco14.slots,
        Reco15.slots,
        StrippingTests.slots,
        Temporary.slots,
        UpgradeTests.slots,
        Release.slots,
    ))
