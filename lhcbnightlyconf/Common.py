# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Common settings
'''
from LbNightlyTools.Configuration import Project

ALL_PROJECTS = [
    'LCG',
    'Gaudi',
    'Online',
    'LHCb',
    'Lbcom',
    'Boole',
    'Rec',
    'Brunel',
    'Phys',
    'Moore',
    'Analysis',
    'Stripping',
    'DaVinci',
    'Panoramix',
    'Bender',
    'MooreOnline',
    'Panoptes',
    'Alignment',
    'Geant4',
    'Gauss',
    'Urania',
    'Castelao',
    'Noether',
    'Kepler',
    'Allen',
    'AlignmentOnline',
    'Lovell',
    'Orwell',
    'LHCbIntegrationTests',
]

DEFAULT_DBASE_PACKAGES = [
    ('PRConfig', 'HEAD'),
]

# basic set of production platforms
BASE_PLATFORMS = [
    'x86_64-centos7-gcc9-opt',
    'x86_64-centos7-gcc9-dbg',
    'x86_64-centos7-clang8-opt',
    'x86_64-centos7-clang8-dbg',
]
# basic production platform plus special optimizations
ALL_PLATFORMS = BASE_PLATFORMS + [
    'x86_64-centos7-gcc9-do0',
    'x86_64+avx2+fma-centos7-gcc9-opt',
    'skylake_avx512+vecwid256-centos7-gcc9-opt',
]
# temporary set of platforms for LCG 95 based builds
ALL_PLATFORMS_NO_CLANG = [p for p in ALL_PLATFORMS if 'clang' not in p]

DEFAULT_SEARCH_PATH = [
    '/cvmfs/lhcbdev.cern.ch/lib/lcg/releases',
    '${CMAKE_PREFIX_PATH}',
    '${CMTPROJECTPATH}',
    '/cvmfs/sft.cern.ch/lcg/releases',
]

DEFAULT_ENV = ['CMAKE_PREFIX_PATH=' + ':'.join(DEFAULT_SEARCH_PATH)]

# extra CMake cache variables that are (currently) propagated to nightly slots
# relevant for the upgrade.
# - THOR_BUILD_TEST_FUNCTOR_CACHE enables an extra functor cache in Rec that is
#   useful for catching problems with ThOr functors.
DEFAULT_CACHE_ENTRIES = {'THOR_BUILD_TEST_FUNCTOR_CACHE': 'ON'}

WARNINGS_EXCEPTIONS = [
    r'.*/(Boost|ROOT)/.*',
    # CMake developer warning.
    # this is to ignore CMake (>=3.15) warning about "project(Project)"
    # missing in the top level CMakeLists.txt
    r'CMake Warning \(dev\) in CMakeLists\.txt\:',
    r'This warning is for project developers',
]

ERROR_EXCEPTIONS = [
    r'Error\.cc',  # hide false positive from build of Delphes
    r'-Wno-error',  # hide false positive from googletest building with -Wno-error=dangling-else
]

DEFAULT_REQUIRED_EXTERNALS = [
    "AIDA",
    "Boost",
    "CLHEP",
    "CppUnit",
    "eigen",
    "expat",
    "fastjet",
    "fftw",
    "graphviz",
    "GSL",
    "HepMC",
    "HepPDT",
    "libgit2",
    "libunwind",
    "Python",
    "RELAX",
    "ROOT",
    "sqlite",
    "tbb",
    "vdt",
    "XercesC",
    "xqilla",
    "xrootd",
]


class LCGToolchains(Project):
    '''
    Special custom project to add 'lcg-toolchains' to a slot.
    '''
    name = 'lcg-toolchains'
    __checkout__ = ('git', {
        'url':
        'https://gitlab.cern.ch/lhcb-core/lcg-toolchains.git'
    })
    ignore_slot_build_tool = True
    build_tool = 'no_build'

    def __init__(self, name, version, **kwargs):
        Project.__init__(self, name, version, **kwargs)
        self.no_test = True
