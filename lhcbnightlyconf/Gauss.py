# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LbNightlyTools.Configuration import Slot, Project, DBASE, PARAM, Package
from .Common import WARNINGS_EXCEPTIONS, ERROR_EXCEPTIONS

import os
# datetime for rebuildAlways temporary hack
import datetime

# list of defined slots
slots = []

## A few reminder of possible ways to include merge requests
# gSim09Cmake = cloneSlot('lhcb-sim09', 'lhcb-sim09-cmake')
# Project('Gauss', 'Sim09', checkout=('git',{'merges':'all'})) ] )
# Project('Gauss', 'Sim09', checkout=('git',{'merges':10})) ] )
# Project('Gauss', 'Sim09', checkout=('git',{'merges':[10,11,12]})) ] )

# set cmt environment to pick up the nightlies for dbase and param
cmtEnv = [('CMTPROJECTPATH=' + '${CMTPROJECTPATH}' +
           ':/cvmfs/lhcbdev.cern.ch/lib/lcg/releases')]

# do the same for cmake
cmakeEnv = [('CMAKE_PREFIX_PATH=' + ':'.join([
    '/cvmfs/lhcbdev.cern.ch/lib/lcg/releases',
    '/cvmfs/lhcb.cern.ch/lib/lcg/releases',
    '/cvmfs/sft.cern.ch/lcg/releases',
    '${CMAKE_PREFIX_PATH}',
]))]

# to test release build
relEnv = [('CMAKE_PREFIX_PATH=' + ':'.join([
    '/cvmfs/lhcb.cern.ch/lib/lcg/releases',
    '${CMAKE_PREFIX_PATH}',
]))]

DEFAULT_DEPLOYMENT = ['cvmfs']
NO_DEPLOYMENT = []

SIM09_PLATFORMS = [
    'x86_64-slc6-gcc48-opt', 'x86_64-slc6-gcc48-dbg', 'x86_64-slc6-gcc49-opt',
    'x86_64-slc6-gcc49-dbg'
]

# Slot for Sim09 on cvmfs with cmt
gSim09 = Slot(
    name='lhcb-sim09',
    projects=[
        Project('LHCb', 'sim09-patches', checkout=('git', {
            'merges': 'all'
        })),
        Project('Gauss', 'Sim09', checkout=('git', {
            'merges': 'all'
        })),
        DBASE(packages=[Package('PRConfig', 'HEAD')]),
        PARAM(packages=[Package('ParamFiles', 'HEAD')])
    ])
gSim09.desc = 'To build Gauss Sim09 with LHCb v39r1p4+patches, Gaudi v26r4 and LCG_79'
gSim09.platforms = SIM09_PLATFORMS
gSim09.build_tool = 'cmt'
gSim09.env = cmtEnv
gSim09.deployment = DEFAULT_DEPLOYMENT
gSim09.cache_entries['GEANT4_TAG'] = 'lhcb/v9.6.4-branch'
#gSim09.cache_entries['GEANT4_TAG'] = 'lhcb/v96r4p3'
#gSim09.env = cmtEnv + ['VERBOSE=1']
slots.append(gSim09)

# Slots for g4 and gauss with cmake
#    for Sim09
gSim09Cmake = Slot(
    name='lhcb-sim09-cmake',
    projects=[
        Project('LCGCMT', '79', disabled=True),
        Project('Geant4', 'Sim09', checkout=('git', {
            'merges': 'all'
        })),
        Project('LHCb', 'sim09-patches', checkout=('git', {
            'merges': 'all'
        })),
        Project('Gauss', 'Sim09', checkout=('git', {
            'merges': 'all'
        })),
        DBASE(packages=[Package('PRConfig', 'HEAD')]),
        PARAM(packages=[Package('ParamFiles', 'HEAD')])
    ])
gSim09Cmake.desc = 'Identical to lhcb-sim09 but built with cmake'
gSim09Cmake.platforms = SIM09_PLATFORMS
gSim09Cmake.warning_exceptions = list(WARNINGS_EXCEPTIONS)
gSim09Cmake.error_exceptions = list(ERROR_EXCEPTIONS)
gSim09Cmake.build_tool = 'cmake'
gSim09Cmake.env = cmakeEnv
gSim09Cmake.deployment = DEFAULT_DEPLOYMENT
gSim09Cmake.cache_entries['GEANT4_TAG'] = 'lhcb/v96r4p3'
gSim09Cmake.cache_entries['GAUDI_DIAGNOSTICS_COLOR'] = False
gSim09Cmake.cache_entries['GAUDI_DIAGNOTICS_COLOR'] = False
# Temp hack to trigger slot rebuild
gSim09Cmake.metadata = {'rebuildAlways': str(datetime.date.today())}
slots.append(gSim09Cmake)

# Special slot for gauss for upgrade study
gSim09Up = Slot(
    name='lhcb-sim09-upgrade',
    projects=[
        Project('LHCb', '2017-patches', checkout=('git', {
            'merges': 'all'
        })),
        Project('Geant4', 'Sim09', checkout=('git', {
            'merges': 'all'
        })),
        Project('Gauss', 'Sim09-upgrade', checkout=('git', {
            'merges': 'all'
        })),
        DBASE(packages=[Package('PRConfig', 'HEAD')]),
        PARAM(packages=[Package('ParamFiles', 'HEAD')])
    ])
gSim09Up.desc = 'Build of Gauss for Upgrade studies, identical to Gauss Sim09 for generators and Geant4, build with LHCb v42r6p2+patches, Gaudi v28r1 and LCG_88'
gSim09Up.platforms = ['x86_64-slc6-gcc49-opt', 'x86_64-slc6-gcc49-dbg']
gSim09Up.warning_exceptions = list(WARNINGS_EXCEPTIONS)
gSim09Up.error_exceptions = list(ERROR_EXCEPTIONS)
gSim09Up.build_tool = 'cmake'
gSim09Up.env = cmakeEnv
gSim09Up.deployment = NO_DEPLOYMENT
gSim09Up.cache_entries['GEANT4_TAG'] = 'lhcb/v9.6.4-branch'
slots.append(gSim09Up)

# Temporary definition of gcc8 platforms for Sim10 until we may include the gcc9 platforms
CORE_PLATFORMS = [
    'x86_64-centos7-gcc9-opt',
    'x86_64-centos7-gcc9-dbg',
]
SIM10_PLATFORMS = [
    'x86_64-centos7-gcc8-opt',
    'x86_64-centos7-gcc8-dbg',
]
DEV_PLATFORMS = CORE_PLATFORMS + []


def create_sim_slot(name, desc, platforms=CORE_PLATFORMS, deploy=False):
    slot = Slot(
        name=name,
        projects=[
            Project('LCG', '96b', disabled=True),
            Project('LHCb', 'v50r6p1', disabled=True),
            Project('Geant4', 'Sim10', checkout=('git', {
                'merges': 'all'
            })),
            Project('Gauss', 'HEAD'),
            DBASE(packages=[Package('PRConfig', 'HEAD')])
        ],
        warning_exceptions=list(WARNINGS_EXCEPTIONS),
        error_exceptions=list(ERROR_EXCEPTIONS))
    slot.desc = desc
    slot.platforms = platforms
    slot.build_tool = 'cmake'
    slot.env = cmakeEnv
    slot.deployment = DEFAULT_DEPLOYMENT if deploy else NO_DEPLOYMENT
    return slot


# Slot for Gauss Sim10 development - for now master, eventually will move to Sim10 branch
gaussSim10 = create_sim_slot(
    'lhcb-sim10',
    'Development build of Gauss for Sim10 with LHCb v50r5+patches, Gaudi v32r1 and LCG_96, Geant4 v104r3p2',
    platforms=SIM10_PLATFORMS,
    deploy=True)
# Temp hack to trigger slot rebuild
gaussSim10.metadata = {'rebuildAlways': str(datetime.date.today())}
slots.append(gaussSim10)

# Slot for Gauss generator development - for now master, eventually will move to Sim10 branch
gaussGen = create_sim_slot(
    'lhcb-gauss-gen-dev',
    'Development build of Gauss identical to Sim10 but with new generators or generator version',
    platforms=CORE_PLATFORMS,
    deploy=False)
slots.append(gaussGen)

# Slot for Gauss developments on latest stable LCG / LHCb
gaussDev = create_sim_slot(
    'lhcb-gauss-dev',
    'Development build of Gauss that could be incompatible with Sim10, using Gaudi v32r2, LCG_96b',
    platforms=DEV_PLATFORMS,
    deploy=True)
# gaussDev.projects.append(Project('Gaudi', 'v32r2', disabled=True))
# gaussDev.LCG.version = '96b'
# gaussDev.LHCb.version = 'HEAD'
# gaussDev.LHCb.disabled = False
# Temp hack to trigger slot rebuild
gaussDev.metadata = {'rebuildAlways': str(datetime.date.today())}
slots.append(gaussDev)

# Slot for Gauss with Lamarr development
gaussLamarr = create_sim_slot(
    'lhcb-gauss-lamarr',
    'Development build of Gauss identical to lhcb-gauss-dev but with Lamarr',
    platforms=CORE_PLATFORMS,
    deploy=True)
gaussLamarr.Gauss.checkout_opts['merges'] = ['all', 516]
slots.append(gaussLamarr)

# Slot for Gaussino and Gauss-on-Gaussino
gaussino = Slot(
    name='lhcb-gaussino',
    projects=[
        Project('LCG', '96b', disabled=True),
        Project('Gaudi', 'master'),
        Project('Geant4', 'v104r3p2', checkout=('git', {
            'merges': 50
        })),
        Project('LHCb', 'modern_sim_components'),
        Project(
            'Gaussino',
            'HEAD',
            checkout=('git', {
                'url': 'https://gitlab.cern.ch/Gaussino/Gaussino.git'
            })),
        Project('Gauss', 'Futurev3', checkout=('git', {
            'merges': 'all'
        })),
        DBASE(packages=[Package('PRConfig', 'HEAD')]),
    ],
    warning_exceptions=list(WARNINGS_EXCEPTIONS),
    error_exceptions=list(ERROR_EXCEPTIONS))
gaussino.desc = 'Test build of Gaussino and future Gauss'
gaussino.platforms = ['x86_64-centos7-gcc8-opt', 'x86_64-centos7-gcc9-opt']
gaussino.build_tool = 'cmake'
gaussino.deployment = DEFAULT_DEPLOYMENT
gaussino.env = cmakeEnv
slots.append(gaussino)
