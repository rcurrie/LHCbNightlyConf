# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LbNightlyTools.Configuration import Slot, Project, DBASE, Package
import os

from .Common import (DEFAULT_ENV, WARNINGS_EXCEPTIONS, ERROR_EXCEPTIONS)

from .Main import create_head_slot

slots = []


# Special slot dedicated to running the timing testsfor the hackathon
def prepareUpgradeTestSlot(slot_name,
                           description,
                           tag,
                           slot_disabled=False,
                           project_names=None):
    """ Create a test slot taking the projects with the specified tag/branch   """

    if project_names == None:
        project_names = ['LHCb', 'Lbcom', 'Rec', 'Brunel', 'Phys', 'Moore']

    # Peparing commin projects
    lcg = Project('LCG', '96b', disabled=True)
    gaudi = Project('Gaudi', 'HEAD', disabled=False)
    projects = [lcg, gaudi]

    # Now adding the ones with the "tag" version
    projects += [Project(name, tag, disabled=False) for name in project_names]

    # Now creating the slot, taking the same defaults as for lhcb-gaudi-head
    uts = Slot(
        slot_name,
        desc=description,
        projects=projects,
        platforms=[
            'x86_64-centos7-gcc9-opt', 'x86_64+avx2+fma-centos7-gcc9-opt'
        ],
        env=list(DEFAULT_ENV),
        deployment=['cvmfs'],
        warning_exceptions=list(WARNINGS_EXCEPTIONS),
        error_exceptions=list(ERROR_EXCEPTIONS),
        disabled=slot_disabled)

    # Adding HEAD of PRConfig data package
    uts.projects.append(DBASE(packages=[
        Package('PRConfig', 'HEAD'),
    ]))

    return uts


# Add slot for throughput testing
throughput = prepareUpgradeTestSlot(
    'lhcb-test-throughput', 'slot for dedicated throughput testing', 'master')
throughput.LCG.version = '96b'
throughput.Gaudi.version = 'master'
for p in throughput.projects:
    if p.name not in ('LCG', 'DBASE', 'PARAM'):
        p.checkout_opts['merges'] = ['label=lhcb-test-throughput']
throughput.platforms = [
    'x86_64-centos7-gcc9-opt',
    'x86_64+avx2+fma-centos7-gcc9-opt',
]
slots.append(throughput)

throughput2 = Slot(
    'lhcb-test-throughput2',
    desc=
    'second throughput slot, simply gaudi-head for less projects, less platforms and not deployed',
    projects=[
        Project(name, 'HEAD') for name in
        ['LCG', 'Gaudi', 'LHCb', 'Lbcom', 'Rec', 'Brunel', 'Phys', 'Moore']
    ],
    platforms=['x86_64-centos7-gcc9-opt', 'x86_64+avx2+fma-centos7-gcc9-opt'],
    env=list(DEFAULT_ENV),
    deployment=[],
    warning_exceptions=list(WARNINGS_EXCEPTIONS),
    error_exceptions=list(ERROR_EXCEPTIONS))
throughput2.LCG.version = '96b'
throughput2.LCG.disabled = True
throughput2.env.append(
    'CMAKE_PREFIX_PATH=${{PWD}}/build/GAUDI/GAUDI_{}/cmake:${{CMAKE_PREFIX_PATH}}'
    .format(throughput2.Gaudi.version))
throughput2.projects.append(DBASE(packages=[Package('PRConfig', 'HEAD')]))
slots.append(throughput2)
