###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LbNightlyTools.Configuration import Slot, Project
from .Common import (ALL_PROJECTS, BASE_PLATFORMS, WARNINGS_EXCEPTIONS,
                     ERROR_EXCEPTIONS, LCGToolchains)
from .Main import LCG_DEFAULT_VERSION

slots = []

# Preparing new JobOptionsSvc interface

MRs = {
    'LHCb': 1714,
}

s = Slot(
    'prepare-gaudi-new-jos',
    disabled=True,
    desc=
    'Test patches for <a href="https://gitlab.cern.ch/gaudi/Gaudi/merge_requests/577">gaudi/Gaudi!577</a>',
    projects=[Project(name, 'master') for name in ALL_PROJECTS],
    platforms=['x86_64-centos7-gcc7-opt'],
    no_test=False)

s.LCG.version = '94'
s.LCG.disabled = True

s.Gaudi.version = 'joboptssvc-redesign'
s.Gaudi.checkout_opts['url'] = 'https://gitlab.cern.ch/clemenci/Gaudi.git'

s.Online.disabled = True

for p in s.projects:
    if p.name in MRs:
        p.checkout_opts['merges'] = [MRs[p.name]]

slots.append(s)

s = Slot(
    'test-new-nightlies',
    disabled=True,
    desc=
    'Testing for new nightlies infrastructure - <a href="https://its.cern.ch/jira/browse/LBCORE-1702"> Details Here </a>',
    projects=[Project('Gaudi', 'master'),
              Project('LHCb', 'master')],
    platforms=['x86_64-centos7-gcc8-opt'],
)
s.Gaudi.checkout_opts['commit'] = '91808eea9f09abd3fc8b619839a7010cd99b561c'
s.LHCb.checkout_opts['commit'] = 'f83f3f076c930f413f105be41fcc41feaeff5e51'
slots.append(s)

s = Slot(
    'lhcb-new-cmake',
    desc='Test slot to play with new CMake configuration',
    projects=[
        LCGToolchains('master'),
        Project('LCG', LCG_DEFAULT_VERSION, disabled=True),
        Project('Gaudi', 'cmake-modernisation'),
        Project('LHCb', 'master'),
        Project('Online', 'master'),
        Project('Lbcom', 'master'),
        Project('Rec', 'master'),
        Project('Brunel', 'master'),
        Project('Phys', 'master'),
        Project('Moore', 'master'),
    ],
    platforms=BASE_PLATFORMS,
    env=[
        'PATH=/cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.16.2/Linux-x86_64/bin:$PATH'
    ],
    warning_exceptions=list(WARNINGS_EXCEPTIONS),
    error_exceptions=list(ERROR_EXCEPTIONS),
)

s.Gaudi.checkout_opts['url'] = 'https://gitlab.cern.ch/clemenci/Gaudi.git'

s.cache_entries['GAUDI_USE_INTELAMPLIFIER'] = True
s.cache_entries['GAUDI_LEGACY_CMAKE_SUPPORT'] = True

slots.append(s)

del s
