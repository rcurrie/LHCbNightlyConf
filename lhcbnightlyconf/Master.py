###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from .Common import ALL_PROJECTS
from .Main import create_head_slot

master = create_head_slot('lhcb-master', projects=ALL_PROJECTS)
master.desc = 'master of everything (without merge requests)'
master.platforms = [
    'x86_64-centos7-gcc9-opt', 'x86_64-centos7-gcc9-dbg', 'x86_64+avx2+fma-centos7-gcc9-opt',
    'x86_64-centos7-clang8-opt', 'x86_64-centos7-clang8-dbg'
]
for project_name in ALL_PROJECTS:
    if project_name != 'LCG':
        getattr(master, project_name).version = 'master'

slots = [master]
