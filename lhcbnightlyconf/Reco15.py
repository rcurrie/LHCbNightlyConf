# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2019-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LbNightlyTools.Configuration import Slot, Project, DBASE, Package
from .Common import WARNINGS_EXCEPTIONS, ERROR_EXCEPTIONS

from itertools import starmap

reco15 = Slot(
    'lhcb-reco15-patches',
    desc='preparation of a patched version of Brunel for Reco15 ',
    projects=starmap(Project, [('LHCb', 'reco15-patches'),
                               ('Lbcom', 'reco15-patches'),
                               ('Rec', 'reco15-patches'),
                               ('Brunel', 'reco15-patches')]),
    platforms=['x86_64-slc6-gcc49-opt', 'x86_64-slc6-gcc49-dbg'],
    warning_exceptions=list(WARNINGS_EXCEPTIONS),
    error_exceptions=list(ERROR_EXCEPTIONS),
    build_tool='cmake',
    deployment=[])

for p in reco15.projects:
    p.checkout_opts['merges'] = 'all'

reco15.projects.append(DBASE(packages=[Package('PRConfig', 'HEAD')]))

slots = [reco15]
