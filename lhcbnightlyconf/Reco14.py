# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LbNightlyTools.Configuration import Slot, Project, DBASE, Package

from itertools import starmap

reco14 = Slot(
    'lhcb-reco14-patches',
    desc='preparation of a patched version of Brunel for Reco14 '
    '(see <a href="https://its.cern.ch/jira/browse/LHCBGAUSS-1065">LHCBGAUSS-1065</a>)',
    projects=starmap(Project, [('LHCb', 'reco14-patches'),
                               ('Lbcom', 'reco14-patches'),
                               ('Rec', 'reco14-patches'),
                               ('Brunel', 'reco14-patches')]),
    platforms=['x86_64-slc5-gcc46-opt', 'x86_64-slc5-gcc46-dbg'],
    build_tool='cmt',
    deployment=[])

for p in reco14.projects:
    p.checkout_opts['merges'] = 'all'

reco14.projects.append(DBASE(packages=[Package('PRConfig', 'HEAD')]))

slots = [reco14]
