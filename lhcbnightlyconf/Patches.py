# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LbNightlyTools.Configuration import Slot, Project, DBASE, PARAM, Package, slot_factory
from .Common import BASE_PLATFORMS, WARNINGS_EXCEPTIONS, ERROR_EXCEPTIONS
from .Main import LCG_DEFAULT_VERSION

NAMES = [
    'LHCb', 'Lbcom', 'Rec', 'Brunel', 'Phys', 'Stripping', 'Analysis',
    'DaVinci', 'Bender'
]

EXTRA_NAMES = {
    '2017': ['Gaudi', 'Hlt', 'Moore', 'LHCbIntegrationTests'],
    '2018': ['Gaudi', 'Hlt', 'Moore', 'LHCbIntegrationTests'],
    'run2': [
        'LCG', 'Gaudi', 'Boole', 'Alignment', 'Panoramix', 'Panoptes',
        'Castelao'
    ]
}

SPECIAL_VERSIONS = {
    '2016': {},
    '2017': {
        'Gaudi': 'v28r2-patches'
    },
    '2018': {
        'Bender': 'run2-patches',
        'Gaudi': 'v29-patches',
    },
    'run2': {
        'LCG': LCG_DEFAULT_VERSION,
        'Gaudi': 'master',
        'LHCbIntegrationTests': 'HEAD',
        'Orwell': 'HEAD',
    }
}

PLATFORMS = {
    '2016': ['x86_64-slc6-gcc49-opt', 'x86_64-slc6-gcc49-dbg'],
    '2017': [
        'x86_64-slc6-gcc49-opt', 'x86_64-slc6-gcc49-dbg',
        'x86_64-slc6-gcc62-opt', 'x86_64-slc6-gcc62-dbg',
        'x86_64-centos7-gcc62-opt', 'x86_64-centos7-gcc62-dbg'
    ],
    '2018': [
        'x86_64-slc6-gcc62-opt', 'x86_64-slc6-gcc62-dbg',
        'x86_64-centos7-gcc62-opt', 'x86_64-centos7-gcc62-dbg',
        'x86_64-centos7-gcc7-opt', 'x86_64-centos7-gcc7-dbg'
    ],
    'run2':
    BASE_PLATFORMS,
}


@slot_factory
def preparePatchesSlot(year, name=None, LCG=None, deploy=True):
    LCG = LCG or SPECIAL_VERSIONS[year].get('LCG', LCG_DEFAULT_VERSION)

    slot = Slot(
        name if name else 'lhcb-{0}-patches'.format(year),
        desc='Test slot with patches for {0} production stack'.format(year),
        projects=[
            Project(
                name,
                SPECIAL_VERSIONS[year].get(name, year + '-patches'),
                checkout=('git', {
                    'merges': 'all'
                })) for name in NAMES + EXTRA_NAMES.get(year, [])
        ],
        platforms=PLATFORMS[year],
        warning_exceptions=list(WARNINGS_EXCEPTIONS),
        error_exceptions=list(ERROR_EXCEPTIONS),
        env=[],
        deployment=['cvmfs'] if deploy else [])

    if hasattr(slot, 'LCG'):
        slot.LCG.version = LCG
        slot.LCG.disabled = True

    # this is needed to make LHCbIntegrationTests find the right vesions
    for project in slot.projects:
        if project.version == 'master':
            # ignore MRs for master
            project.checkout_opts = {}
        slot.cache_entries[project.name + '_version'] = project.version
    # setup data packages
    slot.projects.append(
        DBASE(packages=[
            Package('PRConfig', 'HEAD'),
            Package('AppConfig', 'HEAD')
        ]))
    slot.projects.append(PARAM(packages=[Package('ParamFiles', 'HEAD')]))
    if year in ('2018', 'run2'):
        slot.DBASE.packages.extend([
            Package('TurboStreamProd', 'HEAD'),
            Package('TCK/L0TCK', 'HEAD'),
        ])
    return slot


slots = [preparePatchesSlot(t) for t in ['2016', '2017', '2018', 'run2']]
run2 = slots[-1]

run2.LCG.disabled = True
