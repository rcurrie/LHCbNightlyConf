# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Conguration of Geant4 relase 10 slots
"""
from LbNightlyTools.Configuration import Slot, Project, DBASE, PARAM, Package
from .Common import WARNINGS_EXCEPTIONS, ERROR_EXCEPTIONS
# datetime for rebuildAlways temporary hack
import datetime

cmakeEnv = [('CMAKE_PREFIX_PATH=' + ':'.join([
    '/cvmfs/lhcbdev.cern.ch/lib/lcg/releases',
    '/cvmfs/lhcb.cern.ch/lib/lcg/releases',
    '/cvmfs/sft.cern.ch/lcg/releases',
    '${CMAKE_PREFIX_PATH}',
]))]

# Special slot for geant4 release 10
#g4r103 = Slot(name='lhcb-g4r103',
#              projects=[Project('LCGCMT', '88', disabled=True),
#                        Project('Gaudi', 'v28r2', disabled=True),
#                        Project('LHCb', 'v42r6p2', disabled=True),
#                        Project('Geant4', 'HEAD', checkout=('git', {'merges': [24]})),
#                        Project('Gauss', 'HEAD', checkout=('git', {'merges': [113]})),
#                        DBASE(packages=[Package('PRConfig', 'HEAD')]),
#                        PARAM(packages=[Package('Geant4Files', 'v104r0')])],
#              build_tool='cmake')

g4dev = Slot(
    name='lhcb-g4-dev',
    projects=[
        Project('LCG', '96b', disabled=True),
        Project('LHCb', 'v50r6p1', disabled=True),
        Project('Geant4', 'HEAD'),
        Project('Gauss', 'HEAD'),
        DBASE(packages=[Package('PRConfig', 'HEAD')])
        # PARAM(packages=[Package('Geant4Files', 'v106r0')])
    ],
    warning_exceptions=list(WARNINGS_EXCEPTIONS),
    error_exceptions=list(ERROR_EXCEPTIONS),
    build_tool='cmake')

g4dev.desc = 'Build of Geant4 release 10.x for LHCb'
g4dev.cache_entries = {
    'CMAKE_USE_CCACHE': True,
    'GEANT4_TAG': 'lhcb/v10.6.0-branch'
}
g4dev.platforms = ['x86_64-centos7-gcc9-opt']
g4dev.build_tool = 'cmake'
g4dev.env = cmakeEnv
# + [
#  'USolids_DIR=/cvmfs/lhcbdev.cern.ch/custom/VecGeom/v00.03.01-vc/x86_64-centos7-gcc62-opt/lib/CMake/USolids'
# ]
g4dev.deployment = []
# Temp hack to trigger slot rebuild
g4dev.metadata = {'rebuildAlways': str(datetime.date.today())}

slots = [g4dev]
