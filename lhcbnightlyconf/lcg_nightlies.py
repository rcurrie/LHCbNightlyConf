# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LbNightlyTools.Configuration import Slot, Project, DBASE, Package, slot_factory
from .Common import DEFAULT_REQUIRED_EXTERNALS, BASE_PLATFORMS, DEFAULT_DBASE_PACKAGES, WARNINGS_EXCEPTIONS, ERROR_EXCEPTIONS
from .Main import create_dd4hep_slot, create_head_slot
from .Patches import preparePatchesSlot
from itertools import starmap
import datetime
import os
import re

# list of defined slots
slots = []

PROJECTS = [
    ('Gaudi', 'HEAD'),
    ('Online', 'HEAD'),
    ('LHCb', 'HEAD'),
    ('Lbcom', 'HEAD'),
    ('Boole', 'HEAD'),
    ('Rec', 'HEAD'),
    ('Brunel', 'HEAD'),
    ('Phys', 'HEAD'),
    ('Analysis', 'HEAD'),
    ('Stripping', 'HEAD'),
    ('DaVinci', 'HEAD'),
    ('Panoramix', 'HEAD'),
    ('Bender', 'HEAD'),
    ('Moore', 'HEAD'),
    ('Panoptes', 'HEAD'),
    ('Geant4', 'HEAD'),
    ('Gauss', 'HEAD'),
    ('Urania', 'HEAD'),
    ('Kepler', 'HEAD'),
    ('MooreOnline', 'HEAD'),
]

TODAY = [
    'Sun',
    'Mon',
    'Tue',
    'Wed',
    'Thu',
    'Fri',
    'Sat',
    'Sun',
][datetime.date.today().isoweekday()]


def cmake_prefix_path(name):
    return 'CMAKE_PREFIX_PATH={0}'.format(':'.join([
        '/cvmfs/sft-nightlies.cern.ch/lcg/nightlies/{0}/{1}'.format(
            name, TODAY), '${CMAKE_PREFIX_PATH}'
    ]))


def set_desc(slot, root_version):
    slot.desc = (
        'head of everything against Gaudi <a href="https://gitlab.cern.ch/gaudi/Gaudi/tree/{gaudi_version}" target="_blank">{gaudi_version}</a> '
        'and today\'s LCG <a href="http://lcginfo.cern.ch/release/{name}/" target="_blank">{name}</a> slot '
        '(ROOT {root_version}, <a href="http://cdash.cern.ch/index.php?project=LCGSoft" target="_blank">status</a>)'
    ).format(
        gaudi_version=slot.Gaudi.version,
        name=slot.name.split('-')[-1],
        root_version=root_version)


lcg = dict((
    n,
    Slot(
        'lhcb-lcg-{0}'.format(n),
        projects=[Project('LCG', n, disabled=True)] +
        list(starmap(Project, PROJECTS)),
        deployment=['cvmfs'],
        env=[
            cmake_prefix_path(n),
            'PATH=/cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.16.2/Linux-x86_64/bin:${PATH}'
        ],
        cache_entries={'CMAKE_USE_CCACHE': False},
        preconditions=[{
            "name": "lcgNightly",
            "args": {
                "path":
                '/cvmfs/sft-nightlies.cern.ch/lcg/nightlies/{0}/{1}/LCG_externals_${{CMTCONFIG}}.txt'
                .format(n, TODAY),
                "required":
                DEFAULT_REQUIRED_EXTERNALS
            }
        }],
        warning_exceptions=list(WARNINGS_EXCEPTIONS),
        error_exceptions=list(ERROR_EXCEPTIONS))) for n in ('dev3', 'dev4'))

set_desc(lcg['dev3'], root_version='master')
set_desc(lcg['dev4'], root_version='6.18-patches')

lcg['dev3'].platforms = BASE_PLATFORMS
lcg['dev4'].platforms = list(lcg['dev3'].platforms)

lcg['dev3'].projects.append(
    DBASE(packages=[
        Package(name, version) for name, version in DEFAULT_DBASE_PACKAGES
    ]))
lcg['dev4'].projects.append(
    DBASE(packages=[
        Package(name, version) for name, version in DEFAULT_DBASE_PACKAGES
    ]))

slots.extend(lcg.values())

# Adding slot for DD4hep, based on the dev4 LCG nightly slot
dd4hep_dev4 = create_dd4hep_slot('lhcb-dd4hep-dev4', deploy=False)
dd4hep_dev4.env = dd4hep_dev4.env + lcg['dev4'].env
dd4hep_dev4.cache_entries = lcg['dev4'].cache_entries
dd4hep_dev4.preconditions = lcg['dev4'].preconditions
dd4hep_dev4.enabled = False
slots.append(dd4hep_dev4)

# Adding slot for run2-patches, based on the dev4 LCG nightly slot and Gaudi/HEAD
run2_dev4 = preparePatchesSlot(
    'run2', name='lhcb-run2-patches-dev4', LCG='dev4', deploy=True)
run2_dev4.env += lcg[run2_dev4.LCG.version].env
run2_dev4.cache_entries = lcg[run2_dev4.LCG.version].cache_entries
run2_dev4.preconditions = lcg[run2_dev4.LCG.version].preconditions
run2_dev4.Gaudi.version = 'HEAD'
run2_dev4.desc = "clone of run2-patches against next Gaudi and LCG version"
run2_dev4.platforms = list(lcg['dev4'].platforms)
slots.append(run2_dev4)

# find latest rc build
rc_root = '/cvmfs/sft.cern.ch/lcg/releases'
if os.path.isdir(rc_root):
    rc_versions = [
        v for v in os.listdir(rc_root) if re.match(r'LCG_\d*rc\d*$', v)
        and os.path.isdir(os.path.join(rc_root, v))
    ]
    # sort by number
    rc_versions.sort(
        key=lambda v: [int(i) for i in re.findall(r'\d+', v)], reverse=True)
    if rc_versions:  # create the slot only if there's one
        latest_rc = rc_versions[0]
        lcg_rc = create_head_slot(
            'lhcb-lcg-rc',
            LCG=latest_rc[4:],
            platforms=[
                l[14:-4] for l in os.listdir(os.path.join(rc_root, latest_rc))
                if re.match(r'LCG_externals_.*\.txt$', l)
            ])
        lcg_rc.desc = (
            'Special slot based on lhcb-gaudi-head to test LCG rc builds '
            '(current: %s)' % latest_rc)
        lcg_rc.enabled = False
        lcg_rc.env = lcg_rc.env + [
            'CMAKE_PREFIX_PATH=%s:${CMAKE_PREFIX_PATH}' % rc_root
        ]
        slots.append(lcg_rc)


@slot_factory
def test_lcg_version(version, name='lhcb-lcg-test'):
    lcg_root = '/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_' + version
    if os.path.isdir(lcg_root):
        lcg_test = create_head_slot(
            name,
            LCG=version,
            platforms=[
                l[14:-4] for l in os.listdir(lcg_root)
                if re.match(r'LCG_externals_.*\.txt$', l)
            ])
        lcg_test.desc = (
            'Special slot based on lhcb-gaudi-head to test LCG %s' % version)
        lcg_test.enabled = False
        slots.append(lcg_test)
    else:
        import logging
        logging.warning('%s: not a directory: cannot prepare slot %s',
                        lcg_root, name)


test_lcg_version('96b')
